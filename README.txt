This module is an experiment to allow injection of arbitrary render elements
into arbitrary parts of a render array. It works, but has no user interface (and
could use some performance improvements).

The background for this module is a vision for a better help system for
Drupal 8 (see http://drupal.org/node/1095012) and some discussions as to how
make this happen (see http://drupal.org/node/1031972). Parts of these
discussions concerns being able to insert help links just about anywhere on a
Drupal page. The purpose of this module is to allow just that (though it could
be used for inserting any render element).

The core part of the module works with two parameters:
1) A renderable element, such as a contextual link to a help page
2) A string description of where this element should appear in the $page array,
   such as 'content-sidebar_first-system_navigation'.

To make this really useful it must also be possible to use wildcards in the
string description, such as 'content-%%-field_text-und-add_more'. If you would
like to do this, or have ideas on how it could be done, feel free to join.
